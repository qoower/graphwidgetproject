#-------------------------------------------------
#
# Project created by QtCreator 2017-10-11T17:02:50
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = BasicTest
TEMPLATE = app


SOURCES += main.cpp

include( ../../common.pri )
include( ../../app.pri )

LIBS += -lGraphWidget$${LIB_SUFFIX}
