#include <QApplication>
#include <GraphWidget>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    GraphWidget w;
    w.showMaximized();

    return a.exec();
}
