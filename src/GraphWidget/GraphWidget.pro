#-------------------------------------------------
#
# Project created by QtCreator 2017-10-11T17:25:26
#
#-------------------------------------------------


include( ../../common.pri )
include( ../../lib.pri )

QT       += widgets

greaterThan(QT_MAJOR_VERSION, 4): QT += printsupport

TARGET = GraphWidget$${LIB_SUFFIX}
TEMPLATE = lib

DEFINES += GRAPHWIDGET_LIBRARY

SOURCES += graphwidget.cpp \
    qcustomplot.cpp

HEADERS += graphwidget.h\
        graphwidget_global.h \
    qcustomplot.h


