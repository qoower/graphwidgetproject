#include "graphwidget.h"
#include <QVBoxLayout>
#include "qcustomplot.h"

GraphWidget::GraphWidget(QWidget *parent) :
    QWidget(parent),
    m_plotWidget(new QCustomPlot(this))
{
    QVBoxLayout *mainLayout = new QVBoxLayout(this);
    mainLayout->addWidget(m_plotWidget.data());
}
