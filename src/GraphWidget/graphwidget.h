#ifndef GRAPHWIDGET_H
#define GRAPHWIDGET_H

#include "graphwidget_global.h"
#include <QWidget>
#include <QPointer>

class QCustomPlot;

class GRAPHWIDGETSHARED_EXPORT GraphWidget : public QWidget
{
    Q_OBJECT
public:
    GraphWidget(QWidget *parent = 0);
private:
    QPointer<QCustomPlot> m_plotWidget;
};

#endif // GRAPHWIDGET_H
